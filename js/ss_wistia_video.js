/**
 * @file
 * Attaches behaviors for Wistia video.
 */
(function($, Drupal, once) {
  'use strict';

  Drupal.behaviors.wistiaVideo = {
    attach: function (context, settings) {
      $(once('wistiaVideo', '.wistia_video', context)).each(function () {
        var $video = $(this);
        window._wq = window._wq || [];
        _wq.push({ id: $video.data('wistia-video-id'), options: {
            playerColor: settings.player_settings.color,
            videoFoam: settings.player_settings.video_foam,
            playButton: settings.player_settings.display_play_button,
            // videoWidth: settings.player_settings.video_width,
            // videoHeight: settings.player_settings.video_height,
            autoPlay: settings.player_settings.enable_autoplay,
            controlsVisibleOnLoad: settings.player_settings.hide_control_on_load,
          }
        });
      });
    }
  };

})(jQuery, Drupal, once);

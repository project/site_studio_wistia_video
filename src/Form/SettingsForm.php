<?php

namespace Drupal\site_studio_wistia_video\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_studio_wistia_video.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ss_wistia_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_studio_wistia_video.settings');
    // dump($config);
    // $form['token'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Token'),
    //   '#description' => $this->t('Get your token at <em>https://youraccount.wistia.com/account/api</em>. It is not required for read operations.'),
    //   '#maxlength' => 64,
    //   '#size' => 64,
    //   '#default_value' => $config->get('token'),
    // ];

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance'),
      '#open' => TRUE,
    ];
    $form['appearance']['player_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Player color'),
      '#description' => $this->t('Color used for the video and playlist player.'),
      '#default_value' => $config->get('player_color'),
      '#required' => TRUE,
    ];
    // $form['appearance']['video_width'] = [
    //   '#type' => 'number',
    //   '#title' => $this->t('Video width'),
    //   '#default_value' => $config->get('video_width'),
    //   '#required' => TRUE,
    // ];
    // $form['appearance']['video_height'] = [
    //   '#type' => 'number',
    //   '#title' => $this->t('Video height'),
    //   '#default_value' => $config->get('video_height'),
    //   '#required' => TRUE,
    // ];
    $form['appearance']['display_play_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display play button'),
      '#description' => $this->t('Uncheck to use a custom play button.'),
      '#default_value' => $config->get('display_play_button'),
    ];
    $form['appearance']['hide_control_on_load'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Control on Load'),
      '#description' => $this->t('Check this to Hide Control on Load.'),
      '#default_value' => $config->get('hide_control_on_load'),
    ];
    $form['appearance']['enable_autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Autoplay Video'),
      '#description' => $this->t('Check this to autoplay videos.'),
      '#default_value' => $config->get('enable_autoplay'),
    ];

    // @todo add (optional) default player button image.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('site_studio_wistia_video.settings')
      ->set('token', $form_state->getValue('token'))
      ->set('player_color', $form_state->getValue('player_color'))
      ->set('hide_control_on_load', $form_state->getValue('hide_control_on_load'))
      ->set('enable_autoplay', $form_state->getValue('enable_autoplay'))
      ->set('display_play_button', $form_state->getValue('display_play_button'))
      ->save();
  }

}

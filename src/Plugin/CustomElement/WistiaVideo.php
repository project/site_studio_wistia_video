<?php

namespace Drupal\site_studio_wistia_video\Plugin\CustomElement;
 
use Drupal\cohesion_elements\CustomElementPluginBase;

/**
 * Custom element plugin for filtered views Acquia Cohesion.
 *
 * @CustomElement(
 *   id = "wistia_video",
 *   label = @Translation("Wistia Video")
 * )
 */
class WistiaVideo extends CustomElementPluginBase {
  public function getFields() {
    return [
      'wistia_video_url' => [
        'title' => 'Wistia Video URL',
        'type' => 'textfield',
        'required' => TRUE,
        'validationMessage' => 'This field is required.',
        "description" => 'https://my_account.wistia.com/medias/id',
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class, $element_context = []) {

    $video_url = $element_settings['wistia_video_url'];
    $settings = [];
    $video_id = '';
    if(!empty($video_url)){
      $video_id = $this->getIdFromInput($video_url);
      $settings += $this->getGlobalPlayerSettings();
    }
    // exit;
    // Render the element.
    return [
      '#theme' => 'wistia_video_element_template',
      '#elementSettings' => $element_settings,
      '#elementMarkup' => $element_markup,
      '#elementClass' => $element_class,
      '#video_id' => $video_id,
      '#attached' => [
        'library' => [
          'site_studio_wistia_video/ss_wistia_video',
        ],
        'drupalSettings' => $settings,
      ],
    ];
  }

   /**
   * Returns player settings from the system wide configuration.
   *
   * @return array
   *   Array containing player settings as a key.
   */
  protected function getGlobalPlayerSettings() {
    $globalConfig = \Drupal::configFactory()->get('site_studio_wistia_video.settings');
    return [
      'player_settings' => [
        'video_foam' => 'true',
        'color' => $globalConfig->get('player_color'),
        'display_play_button' => $globalConfig->get('display_play_button') === 1 ? 'true' : 'false',
        'hide_control_on_load' => $globalConfig->get('hide_control_on_load') === 1 ? 'true' : 'false',
        'enable_autoplay' => $globalConfig->get('enable_autoplay') === 1 ? 'true' : 'false',
        // 'video_width' => $globalConfig->get('video_width'),
        // 'video_height' => $globalConfig->get('video_height'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getIdFromInput($input) {
    // Example url: https://my_account.wistia.com/medias/id
    preg_match('/^https?:\/\/(.+)?(wistia.com|wi.st)\/(medias|embed)\/(?<id>[0-9A-Za-z]+)$/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }
}